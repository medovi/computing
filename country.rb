class Country
  attr_accessor :country

  def initialize(name)
    @country = WorldStats.build_dataset.select{|data| data[:country] == name }.shift
  end

  def population
    country[:population]
  end

  def life_expectancy
    country[:life_expectancy]
  end

  def gdp
    country[:gdp]
  end
end
