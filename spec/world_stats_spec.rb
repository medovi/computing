describe WorldStats do
  before do
    dataset = [
      {:country=>"Ethiopia", :gdp=>159, :population=>222, :life_expectancy=>72},
      {:country=>"Moldova", :gdp=>741, :population=>487, :life_expectancy=>67},
      {:country=>"Mauritania", :gdp=>305, :population=>402, :life_expectancy=>78}
    ]
    allow(WorldStats).to receive(:build_dataset).and_return(dataset)
  end

  describe "#gdp" do
    context "when there isn't any data" do
      before do
        allow(WorldStats).to receive(:build_dataset).and_return([])
      end
      it "returns nil" do
        expect(subject.gdp).to be_nil
      end
    end

    it "returns the gdp's mean by default" do
      expect(subject.gdp).to eq(401.67)
    end

    it "returns gdp's median" do
      expect(subject.gdp(calculator: 'median')).to eq(305)
    end
  end

  describe "#population" do
    it "returns population's mean of a subset of countries " do
      countries = ["Panama", "Ethiopia", "Moldova"]
      #487+222/2
      expect(subject.population(countries: countries)).to eq(354.5)
    end
  end

  describe "#life_expectancy" do
    it "returns its mean" do
      expect(subject.life_expectancy).to eq(72.33)
    end
  end

  describe '#mean' do
    it "returs mean info for given countries and metrics" do
      countries = ["Mauritania", "Ethiopia", "Moldova"]
      expect(
        subject.mean(metrics: %w(gdp population), countries: countries )
      ).to eq({gdp: 401.67, population: 370.33})
    end
  end

  describe '#median' do
    it "returs median info for given countries and metrics" do
      countries = ["Mauritania", "Ethiopia", "Moldova"]
      expect(
        subject.median(metrics: %w(gdp life_expectancy population), countries: countries )
      ).to eq({gdp: 305, population: 402, life_expectancy: 72})
    end
  end
end
